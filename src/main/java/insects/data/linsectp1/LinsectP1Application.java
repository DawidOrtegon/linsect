package insects.data.linsectp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinsectP1Application {

    public static void main(String[] args) {
        SpringApplication.run(LinsectP1Application.class, args);
    }

}
