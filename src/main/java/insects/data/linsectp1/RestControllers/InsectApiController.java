package insects.data.linsectp1.RestControllers;

import insects.data.linsectp1.Services.InsectManager;
import insects.data.linsectp1.entities.Insect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(value = "api/insects")
public class InsectApiController
{
    private InsectManager insectManager;

    @Autowired
    public InsectApiController(InsectManager insectManager) {
        this.insectManager = insectManager;
    }

    // Find all the insects.
    @GetMapping("/allInsects")
    public Iterable<Insect> getAllInsects()
    {
        return insectManager.getAllInsects();
    }

    // Find an insect by his Id.
    public Optional<Insect> getInsectById(@RequestParam Long index)
    {
        return insectManager.getInsectById(index);
    }

    // Add a new insect to the table.
    public Insect addInsect(@RequestParam Insect insect)
    {
        return insectManager.addInsect(insect);
    }






}
