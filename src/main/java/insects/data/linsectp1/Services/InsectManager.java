package insects.data.linsectp1.Services;

import insects.data.linsectp1.entities.Insect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.awt.*;
import java.util.Optional;

@Service
public class InsectManager
{
    private InsectRepository insectRepository;

    @Autowired
    public InsectManager(InsectRepository insectRepository) {
        this.insectRepository = insectRepository;
    }

    // Get all the insects.
    public Iterable<Insect> getAllInsects()
    {
        return insectRepository.findAll();
    }

    // Get an insect by his Id.
    public Optional<Insect> getInsectById( Long index)
    {
        return insectRepository.findById(index);
    }

    // Add insect to the table.
    public Insect addInsect(Insect insect)
    {
        return insectRepository.save(insect);
    }

    // Fill the table with the information indicated.
    @EventListener(ApplicationReadyEvent.class)
    public void fillInsectTable()
    {

    }
}
