package insects.data.linsectp1.Services;

import insects.data.linsectp1.entities.Insect;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InsectRepository extends CrudRepository<Insect, Long>
{

}
