package insects.data.linsectp1.entities;

import javax.persistence.*;

@Entity
@Table(name = "Insect")
public class Insect
{
    // Attributes or columns for the table.
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String Name;
    private String Description;

    @Column(length=16777216)
    private byte[] Picture;
    private String Family;
    private String Link;

    // Constructor with all the parameters.
    public Insect(String name, String description, byte[] picture, String family, String link) {
        Name = name;
        Description = description;
        Picture = picture;
        Family = family;
        Link = link;
    }

    // Empty Constructor.
    public Insect() {
    }

    // Setter and getter.

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public byte[] getPicture() {
        return Picture;
    }

    public void setPicture(byte[] picture) {
        Picture = picture;
    }

    public String getFamily() {
        return Family;
    }

    public void setFamily(String family) {
        Family = family;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }
}
